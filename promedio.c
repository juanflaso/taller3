#include <stdio.h>
#include <string.h>

#define TAMANO_MAX 100
#define MAX 50 

void reemplazar(char linea[], char viejo, char nuevo);

int main()
{
	printf("Este programa calcula el promedio de los numeros.\nUse -1 para indicar el final de la lista.\n");
    char line[MAX] = {0};
    float number = 0;


	// se crea la variable acumulador en -1 para compensar la última ejecución del ciclo (cuando se ingresa -1)
 	int acumulador=-1;
	float total = 0;
    printf("Ingrese los números: \n");
    while  (number != -1) {
        fgets(line, MAX, stdin);
        reemplazar(line, '\n', '\0');
        sscanf(line, "%f", &number);
	    total+=number;
        acumulador++; //se acumula las veces que se ingresa un numero
        printf("Ingresó el número: %f\n", number);
        printf("EL total es: %f\n", total);
    }


	//Se suma al total 1 para compensar la entrada de -1 para terminar el ingreso
	total+=1.0;


    // Se imprime el resultado habiendo dividido el total para la cantidad de numeros
    printf("El promedio es %f\n", total/((float)acumulador));
	return 0;
}

//Se reemplaza el último char por el nuevo si se lo encuentra
void reemplazar(char linea[], char viejo, char nuevo) {
    int i = strlen(linea);
    if ((i>0) && (linea[i] == viejo)) {
        linea[i] = nuevo;
    }
}
